const randomPlus = require("./randomPlus");
const random = require("./random");

jest.mock("./random");

describe("Test randomPlus with random mock", () => {
	it("Mock random with 1", () => {
		random.mockImplementation(() => 1);
		expect(randomPlus(100)).toBe(101);
	});
    
	it("Mock random with 99", () => {
		random.mockImplementation(() => 99);
		expect(randomPlus(5)).toBe(104);
	});
});

